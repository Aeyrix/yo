require 'net/http'

class Yo
  attr_accessor :api_token

  def initialize(api_token)
    @@api_token = api_token
  end

  def yo
    Net::HTTP.post_form(URI('http://api.justyo.co/yoall/'), 'api_token' => @@api_token)
  end
end

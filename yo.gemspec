Gem::Specification.new do |s|
  s.name = 'yo-api'
  s.version = '1.1'
  s.authors = 'Elliot Speck (@Aeoxic)'
  s.date = '2014-06-29'
  s.summary = 'Yo!'
  s.description = 'Ruby gem for the Yo API (http://justyo.co/).'
  s.email = 'me@elliot.pro'
  s.homepage = 'http://bitbucket.org/Aeoxic/yo'
  s.license = 'MIT'
  s.files = ['lib/yo.rb', 'LICENSE', 'README.md']
  s.require_paths = ['lib']
end
